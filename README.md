# Ransomware Response Kit
I have compiled this kit to be used for security professionals and 
system administrators alike, in order to help streamline the process of 
responding to ransomware infections.

## Instructions

You should never pay the ransom. This will only reinforce this type of attack. According to most security intelligence reports, criminal enterprises are already making large profits from ransomware.

>In case of infection:

- Remove the impacted system from the network
- Attempt to identify which variant of ransomware you are infected with.
- Before removing the threat, create a copy if possible for later analysis, which may be needed for decryption of files.
- If possible, use restore points or backups to return to a safe state after removing the threat.
- If you have identified the variant of ransomware and a decrypter tool is available for it in this kit, you can attempt to utilize it.



## Contained in this kit
|Directory|Information| 
|-------------|-----------------------------------------------------------|
|\CryptoLocker|Instructions on CryptoLocker removal and threat mitigation|
|\CryptoLockerDecrypt|FireEye Tool to decrypt files encrypted by the CryptoLocker ransomware|
|\TrendMicro_Ransomware_RemovalTool|TrendMicro's ransomware removal tool + instructions|
|\FBIRansomWare|Instructions to remove FBIRansomWare and removal tools|
|\CoinVault|Instructions to remove coinvault ransomware and removal tools|
|\TeslaCrypt|Tool fo removing a variant of cryptolocker ransomware|